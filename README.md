# WebChallenge


## Tech Stack

The application is built using the following technologies:



<div align="center">

![Angular](https://img.shields.io/badge/angular-%23DD0031.svg?style=for-the-badge&logo=angular&logoColor=white)
&nbsp;![Firebase](https://img.shields.io/badge/firebase-%23039BE5.svg?style=for-the-badge&logo=firebase)
&nbsp;![TypeScript](https://img.shields.io/badge/typescript-%23007ACC.svg?style=for-the-badge&logo=typescript&logoColor=white)
&nbsp;![NPM](https://img.shields.io/badge/NPM-%23CB3837.svg?style=for-the-badge&logo=npm&logoColor=white)

</div>


## Quick Start

Follow these steps to run the application locally:

1. Clone the repository to your local machine:
```
$ git clone https://gitlab.com/thusithanuwanb/web-challenge.git
```

2. Change directory to the project folder:
```
$ cd ./WebChallenge7Days
```

3. Install dependencies:
```
$ npm install
```

6. Start the development server:
```
$ ng serve
```
## Screenshots

## Contact

If you have any questions or feedback, please feel free to reach out:

- E-mail: thusithanuwanb@gmail.com
- LinkedIn: [Thusitha Nuwan](www.linkedin.com/in/thusitha-nuwan)

Thank you for visiting this repository! If you find it helpful or inspiring, don't forget to leave a star ⭐️. Happy coding!
