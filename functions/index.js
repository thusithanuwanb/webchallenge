const functions = require("firebase-functions");
const multer = require("multer");
const axios = require("axios");
multer();
const cors = require("cors")({origin: true});

exports.sendToThreatDetect = functions.https.onRequest((req, res) => {
  cors(req, res, ()=> {
    if (req.method !== "POST") {
      return res.status(405).send("Method Not Allowed");
    }

    const subscriptionKey = req.headers["promiseq-subscription-key"];
    if (!subscriptionKey) {
      return res.status(400).send("Missing subscription key header");
    }

    try {
      const imageBuffer = req.body;

      axios.post(
          "https://europe-west1-promiseq-production2.cloudfunctions.net/sendToThreatDetect",
          imageBuffer,
          {
            headers: {
              "Content-Type": req.headers["content-type"],
              "promiseq-subscription-key": subscriptionKey,
            },
          },
      )
          .then((response) => {
            res.status(response.status).send(response.data);
          })
          .catch((error) => {
            console.error("Error:", error);
            res.status(500).send("Internal Server Error");
          });
    } catch (error) {
      console.error("Error:", error);
      res.status(500).send("Internal Server Error");
    }
  });
});
