import {Component, ElementRef, EventEmitter, HostListener, Input, Output, ViewChild} from '@angular/core';

interface Frame {
  position: { x: number, y: number };
  size: { w: number, h: number, pw: number, ph: number };
  typeClass: string
}

@Component({
  selector: 'app-preview',
  templateUrl: './preview.component.html',
  styleUrls: ['./preview.component.scss']
})
export class PreviewComponent {

  @ViewChild('myIdentifier1', {static: false}) myIdentifier1!: ElementRef;
  @Input() imageUrl: string | undefined;
  @Input() isOpen: boolean = false;
  @Input() metadata: any = false;

  @HostListener('document:keydown', ['$event'])
  handleKeyPress(event: KeyboardEvent): void {
    if (event.key === 'Escape' || event.code === 'Escape') {
      this.close();
    }
  }

  frames: Frame[] = [];


  close(): void {
    this.isOpen = false;
  }

  updateImageURLAndMetadata(newImageURL: string, metadata: any): void {
    this.imageUrl = newImageURL;
    this.isOpen = true;
    this.metadata = metadata;
    setTimeout(() => {
      this.drawObject(this.myIdentifier1);
    }, 500)
  }

  deleteFrame(index: number): void {
    this.frames.splice(index, 1);
  }

  drawObject(identifier: any) {
    this.frames = [];
    this.metadata.forEach((entry: any) => {
      const {xmin, ymin, xmax, ymax} = entry.bbox;
      var x = xmin * identifier.nativeElement.offsetWidth;
      var y = ymin * identifier.nativeElement.offsetHeight;
      var w = (xmax - xmin) * identifier.nativeElement.offsetWidth;
      var h = (ymax - ymin) * identifier.nativeElement.offsetHeight;
      var arr = {
        position: {x: x, y: y},
        size: {w: w, h: h, pw: identifier.nativeElement.offsetWidth, ph: identifier.nativeElement.offsetHeight},
        typeClass: entry.class
      };
      this.frames.push(arr);

    });
  }


}
