import {
  Component,
  ElementRef,
  EventEmitter,
  HostListener,
  Inject,
  Input, OnInit,
  Output,
  Renderer2,
  ViewChild
} from '@angular/core';
import {DOCUMENT} from "@angular/common";

export type ResizeAnchorType =
  | 'top'
  | 'left'
  | 'bottom'
  | 'right'

export type ResizeDirectionType =
  | 'x'
  | 'y'
  | 'xy';

@Component({
  selector: 'app-frame',
  templateUrl: './frame.component.html',
  styleUrls: ['./frame.component.scss']
})


export class FrameComponent  {
  @ViewChild('resizeCorner') resizeCornerRef!: ElementRef;
  @ViewChild('wrapper') wrapperRef!: ElementRef;
  @ViewChild('topBar') topBarRef!: ElementRef;


  @Input() position!: { x: number, y: number } ;
  @Input() size!: { w: number, h: number , pw:number, ph:number } ;
  @Input() parentContainerRect!: ClientRect | DOMRect;
  @Input() typeClass!: string;

  @Output() deleteFrame = new EventEmitter<void>();

  lastPosition!: { x: number, y: number };
  lastSize!: { w: number, h: number };

  minSize: { w: number, h: number } = { w: 10, h: 10 };
  private isFocused: boolean = false;

  constructor(@Inject(DOCUMENT) private _document: Document,
              private _el: ElementRef,
              private renderer: Renderer2) { }


  @HostListener('document:keydown', ['$event'])
  handleKeyPress(event: KeyboardEvent): void {
    if (this.isFocused && (event.key === 'Delete' || event.code === 'Delete')) {
      this.deleteFrame.emit();
    }
  }

  getResizerClass(): string {
    if (this.typeClass === 'person') {
      return 'resizerH';
    } else if (this.typeClass === 'heavy_vehicle' || this.typeClass === 'light_vehicle') {
      return 'resizerV';
    } else {
      return 'resizer';
    }
  }


  onFocus(): void {
    const topBarElement = this.wrapperRef.nativeElement.querySelector('.top-bar-wrapper');

    if (this.isFocused && topBarElement){
      this.renderer.setStyle(topBarElement, 'background-color', 'transparent');
      this.isFocused = false;
    } else {
      this.isFocused = true;
      this.renderer.setStyle(topBarElement, 'background-color', 'red');
      this.renderer.setStyle(topBarElement, 'opacity', '0.1');
    }
  }

  startDrag($event:any): void {
    $event.preventDefault();
    const mouseX = $event.clientX;
    const mouseY = $event.clientY;

    const positionX = this.position.x;
    const positionY = this.position.y;

    const duringDrag = (e: any) => {
      const dx = e.clientX - mouseX;
      const dy = e.clientY - mouseY;

      if (this.canDrag(positionX + dx)) {
        this.position.x = positionX + dx;
      }

      if (this.canDragY(positionY + dy)) {
        this.position.y = positionY + dy;
      }

      this.lastPosition = { ...this.position };
    };

    const finishDrag = (e:any) => {
      this._document.removeEventListener('mousemove', duringDrag);
      this._document.removeEventListener('mouseup', finishDrag);
    };

    this._document.addEventListener('mousemove', duringDrag);
    this._document.addEventListener('mouseup', finishDrag);
  }

  canDrag(newX: number): boolean {

    return newX >= 0 && newX <= this.size.pw - this.size.w;
  }

  canDragY(newY: number): boolean {
    return newY >= 0 && newY <= this.size.ph - this.size.h;
  }

  startResize($event: any, anchors: ResizeAnchorType[], direction: ResizeDirectionType): void {
    $event.preventDefault();
    const mouseX = $event.clientX;
    const mouseY = $event.clientY;
    const lastX = this.position.x;
    const lastY = this.position.y;
    const dimensionWidth = this.resizeCornerRef.nativeElement.parentNode.offsetWidth;
    const dimensionHeight = this.resizeCornerRef.nativeElement.parentNode.offsetHeight;

    const positionX = this.position.x;
    const positionY = this.position.y;

    const duringResize = (e: any) => {
      let dw = dimensionWidth;
      let dh = dimensionHeight;
      if (direction === 'x' || direction === 'xy') {
        if (anchors.includes('left')) {
          dw += (mouseX - e.clientX);
          // Check if resizing exceeds the left boundary
          if (dw < 10 || dw < 10 ){
            this._document.removeEventListener('mousemove', duringResize);
            this._document.removeEventListener('mouseup', finishResize);
          }
          if (this.position.x > 0) {
            this.position.x = Math.max(lastX + e.clientX - mouseX, 0);
            this.size.w = Math.max(dw, this.minSize.w);
          }
        } else if (anchors.includes('right')) {
          dw -= (mouseX - e.clientX);
          // Check if resizing exceeds the right boundary
          if (dw < 10 || dw < 10 ){
            this._document.removeEventListener('mousemove', duringResize);
            this._document.removeEventListener('mouseup', finishResize);
          }
          if (this.position.x < this.size.pw - this.size.w) {
            this.size.w = Math.max(dw, this.minSize.w);
          }
        }
      }
      if (direction === 'y' || direction === 'xy') {
        if (anchors.includes('top')) {
          dh += (mouseY - e.clientY);
          // Check if resizing exceeds the top boundary
          if (dw < 10 || dw < 10 ){
            this._document.removeEventListener('mousemove', duringResize);
            this._document.removeEventListener('mouseup', finishResize);
          }
          if (this.position.y > 0) {
            this.position.y = Math.max(lastY + e.clientY - mouseY, 0);
            this.size.h = Math.max(dh, this.minSize.h);
          }
        } else if (anchors.includes('bottom')) {
          dh -= (mouseY - e.clientY);
          if (dw < 10 || dw < 10 ){
            this._document.removeEventListener('mousemove', duringResize);
            this._document.removeEventListener('mouseup', finishResize);
          }
          // Check if resizing exceeds the bottom boundary
          if (this.position.y < this.size.ph - this.size.h) {
            this.size.h = Math.max(dh, this.minSize.h);
          }
        }
      }

      this.lastSize = { ...this.size };
    };

    const finishResize = (e: any) => {
      this._document.removeEventListener('mousemove', duringResize);
      this._document.removeEventListener('mouseup', finishResize);
    };

    this._document.addEventListener('mousemove', duringResize);
    this._document.addEventListener('mouseup', finishResize);
  }

  protected readonly event = event;

}
