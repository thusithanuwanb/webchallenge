import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from "@angular/common/http";

var apiUrl = environment.apiUrl;

var httpLink = {
  postImage: apiUrl + "/sendToThreatDetect"
}

@Injectable({
  providedIn: 'root'
})
export class HttpProviderService {

  constructor(private httpClient: HttpClient) {
  }

  public postImage(image: any, headers: any) {
    return this.httpClient.post(httpLink.postImage, image, headers);
  }

}
