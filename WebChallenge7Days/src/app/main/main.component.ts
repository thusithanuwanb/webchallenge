import {AfterViewInit, Component, ElementRef, HostListener, Renderer2, ViewChild} from '@angular/core';
import {HttpHeaders} from "@angular/common/http";
import {HttpProviderService} from "../services/http-provider.service";
import {PreviewComponent} from "../preview/preview.component";
import * as http from "http";
import {DatePipe} from "@angular/common";

interface Frame {
  position: { x: number, y: number };
  size: { w: number, h: number, pw: number, ph: number };
  typeClass: string
}

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent {

  @ViewChild('dragger') dragger!: ElementRef;
  @ViewChild('dragger_text') draggerText!: ElementRef;
  @ViewChild("browse_btn") browseFileBtn!: ElementRef;
  @ViewChild("file_selector_input") fileSelectorInput!: ElementRef;
  @ViewChild('txt_title') titleInput!: ElementRef;
  @ViewChild('transparencyCheckBox') transparencyCheckBox!: ElementRef;
  @ViewChild('txt_keyword') keywordInput!: ElementRef;
  @ViewChild('spinner') spinner!: ElementRef;
  @ViewChild('upload_btn') uploadBtn!: ElementRef;
  @ViewChild('myIdentifier', {static: false}) myIdentifier!: ElementRef;


  imageFile: File | null = null;
  validFileTypes: string[] = ['jpeg', 'png'];
  validType: boolean = true ;
  isImageAvailable: boolean = false;
  imageURL: string = '';
  title: string = '';
  extensionByName: string = '';
  isInputDisabled: boolean = false;
  realFileType: string = '';
  isExtensionCorrect: boolean = true;
  renameExtensionTo: string = '';
  isUploadFailed: boolean = false;
  isUploadSuccess: boolean = false;
  home: boolean = true;
  show = true;
  metadata: any;
  vehicle: string = "";
  person: string = "";
  id: any;
  createdAt: any;
  result1: string = "No-Person";
  result2: string = "No-Vehicle";
  frames: Frame[] = [];


  constructor(private httpProvider: HttpProviderService, private elementRef: ElementRef, private renderer: Renderer2, private datePipe: DatePipe) {

  }

  fileSelectorChange(event: any) {
    const file = event.target.files[0];
    this.uploadFileHandler(file);
  }

  dragOver(event: DragEvent) {
    event.preventDefault();
    this.draggerText.nativeElement.textContent = 'Release to Upload Image';

  }

  dragLeave() {
    this.draggerText.nativeElement.textContent = 'Drag & Drop Image';
  }

  drop(event: any) {
    event.preventDefault();
    const file = event.dataTransfer.items[0].getAsFile();
    this.uploadFileHandler(file);
    this.draggerText.nativeElement.textContent = 'Drag & Drop Image';

  }


  uploadFileHandler(file: File) {
    this.isUploadSuccess = false;
    this.validType = true;
    this.isExtensionCorrect = true;
    this.isImageAvailable = false;

    this.identifyFileExtensionAndTitleByName(file.name);
    const myPromise = this.identifyFileType(file);

    myPromise.then((value: string) => {
      this.realFileType = value;

      if (!(this.validFileTypes.includes(this.realFileType))) {
        this.extensionByName = '';
        this.title = '';
        this.realFileType = '';
        this.validType = false;

        return;
      }
      this.validType = true;

      this.isExtensionCorrect = this.rejectIfExtensionIncorrect();
      if (!this.isExtensionCorrect) {
        this.extensionByName = '';
        this.title = '';
        this.realFileType = '';
        this.isExtensionCorrect = false;
        this.renameExtensionTo = value;

        return;
      }
      this.isExtensionCorrect = true;
      this.imageFile = file;
      const fileReader: FileReader = new FileReader();

      fileReader.onloadend = (event) => {
        this.removeSelectedImage();
        this.imageURL = fileReader.result as string;
        this.isImageAvailable = true;

      };
      fileReader.readAsDataURL(file);

      if (this.home && !this.validType) {
        this.home = true;
      } else {
        this.home = false;
      }
      this.uploadRequest();
    });
  }

  identifyFileExtensionAndTitleByName(name: string) {
    const index: number = name.lastIndexOf('.');
    if (index >= 0) {
      this.extensionByName = name.substring(index);
      this.title = (name.substring(0, index).length > 200) ? name.substring(0, 200) : name.substring(0, index);
    } else {
      this.title = (name.length > 200) ? name.substring(0, 200) : name;
    }
  }

  async identifyFileType(file: File): Promise<string> {
    return new Promise<string>((resolve, reject) => {
      const reader = new FileReader();
      let fileType: string = '';

      reader.onload = (e) => {
        const buffer = reader.result as ArrayBuffer;
        const magicNumber = new Uint8Array(buffer, 0, 8);

        const magicNumbers: { [key: string]: string } = {
          'ffd8ff': 'jpeg',
          '89504e': 'png',
          '3c3f78': 'svg',
          '524946': 'webp',
          '574542': 'webp'
        };

        const hexMagicNumber = Array.from(magicNumber)
          .map(byte => byte.toString(16).padStart(2, '0'))
          .join('');

        fileType = magicNumbers[hexMagicNumber.substring(0, 6)] || 'Unknown';
        resolve(fileType);
      };
      reader.onerror = (e) => {
        reject('Error : ' + e);

      }
      reader.readAsArrayBuffer(file);
    });
  }

  removeSelectedImage() {
    this.id = "";
    this.createdAt = "";
    this.person = "";
    this.vehicle = "";
    this.clearFrame();
    this.home = false;
    this.imageFile = null;
    this.validType = true;
    this.realFileType = '';
    this.isImageAvailable = false;
    this.imageURL = '';
    this.title = '';
    this.extensionByName = '';
    this.isInputDisabled = false;
    this.isExtensionCorrect = true;
    this.isUploadFailed = false;
    this.id = "";
    this.createdAt = "";
  }


  private rejectIfExtensionIncorrect(): boolean {
    if ((this.extensionByName.length === 1 || 0)) {
      return false;
    } else if (['jpg', 'jfif', 'pjpeg', 'pjp'].includes(this.extensionByName.substring(1))) {
      this.extensionByName = '.jpeg';
    }
    if (this.extensionByName.substring(1) !== this.realFileType) {
      return false;
    }
    return true;
  }


  async uploadRequest() {
    const headers = new HttpHeaders({
      'promiseq-subscription-key': '076d15ec-09e6-4288-a998-88'
    })

    const formData: FormData = new FormData();
    if (this.imageFile) {
      formData.append('image', this.imageFile);
    } else {
      this.removeSelectedImage();
      this.validType = false;
      return;
    }
    this.show = true;
    this.frames = [];
    this.httpProvider.postImage(formData, {headers}).subscribe({
      next: (data: any) => {
        if (data != null) {
          this.id = data.job_id;
          this.createdAt = this.datePipe.transform(data.created_at, 'short');

          this.metadata = data.metadata;
          this.drawObject(this.myIdentifier);
        }
      },
      error: (error: any) => {
        alert("OOPs Something went wrong !")
        console.error("Error:", error);
      },
      complete: () => {
        this.show = false;
      },
    });


  }


  drawObject(identifier: any) {
    this.frames = [];
    var resultSet = new Set();
    this.metadata.forEach((entry: any) => {
      const {xmin, ymin, xmax, ymax} = entry.bbox;
      resultSet.add(entry.class);
      var x = xmin * identifier.nativeElement.offsetWidth;
      var y = ymin * identifier.nativeElement.offsetHeight;
      var w = (xmax - xmin) * identifier.nativeElement.offsetWidth;
      var h = (ymax - ymin) * identifier.nativeElement.offsetHeight;

      var arr = {
        position: {x: x, y: y},
        size: {w: w, h: h, pw: identifier.nativeElement.offsetWidth, ph: identifier.nativeElement.offsetHeight},
        typeClass: entry.class
      };
      this.frames.push(arr);
    });
    this.getResult(resultSet);
  }


  getResult(result: any) {
    const arrayFromSet: string[] = [...result];

    if (arrayFromSet.length === 0) {
      this.person = "No-Person";
      this.vehicle = "No-Vehicle";
      return;
    }

    arrayFromSet.forEach((type) => {
      if (type === "person") {
        this.person = "Person";
      } else if (type === "heavy_vehicle" || type === "light_vehicle") {
        this.vehicle = "Vehicle";
      }
    });

  }


  @HostListener('window:resize', ['$event'])
  onResize(event: any) {
    this.drawObject(this.myIdentifier);
  }


  deleteFrame(index: number): void {
    this.frames.splice(index, 1);
  }

  clearFrame() {
    this.frames = [];
  }


  @ViewChild('previewComponent', {static: false}) previewComponent!: PreviewComponent;

  isOpen: boolean = false;

  @HostListener('document:click', ['$event'])
  handleDocumentClick(event: Event): void {
    if (this.myIdentifier) {
      const clickedInside = this.myIdentifier.nativeElement.contains(event.target);

      if (clickedInside) {
        this.isOpen = true;
        this.previewComponent.updateImageURLAndMetadata(this.imageURL, this.metadata);
      }
    }
  }

  homePage() {
    this.removeSelectedImage();
    this.home = true;
  }


}
